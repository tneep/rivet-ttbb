# BEGIN PLOT /ATLAS_2018_I1705857/*
XTwosidedTicks=1
YTwosidedTicks=1
# LegendXPos=0.7
NormalizeToIntegral=1
LogX=1
LogY=1
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d01-x01-y01
Title=Fiducial cross-sections
YLabel=$\sigma_{\mathrm{fid}}$ [fb]
XMinorTickMarks=0
# Using tabs becasue of the version of make-plots we get from asetup
XCustomMajorTicks=1	$e\mu\geq3b$	2	$e\mu\geq4b$	3	$\ell+\mathrm{jets}\geq3b$	4	$\ell+\mathrm{jets}\geq4b$
NormalizeToIntegral=0
LogX=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d02-x01-y01
Title=Fiducial cross-sections ($t\bar{t}X$ subtracted)
YLabel=$\sigma_{\mathrm{fid}}$ [fb]
XMinorTickMarks=0
# Using tabs becasue of the version of make-plots we get from asetup
XCustomMajorTicks=1	$e\mu\geq3b$	2	$e\mu\geq4b$	3	$\ell+\mathrm{jets}\geq3b$	4	$\ell+\mathrm{jets}\geq4b$
NormalizeToIntegral=0
LogX=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d03-x01-y01
Title=nbjets_emu
XLabel=$N_{b\mathrm{-jets}}$
YLabel=$\frac{1}{\sigma_{t\bar{t}}} \frac{d \sigma_{t\bar{t}}}{d N_{b\mathrm{-jet}}}$
XMinorTickMarks=0
# Using tabs becasue of the version of make-plots we get from asetup
XCustomMajorTicks=1	2	2	3	3	$\geq4$
LogX=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d04-x01-y01
Title=nbjets_emu ($t\bar{t}X$ subtracted)
XLabel=$N_{b\mathrm{-jets}}$
YLabel=$\frac{1}{\sigma_{t\bar{t}}} \frac{d \sigma_{t\bar{t}}}{d N_{b\mathrm{-jet}}}$
# Using tabs becasue of the version of make-plots we get from asetup
XCustomMajorTicks=1	2	2	3	3	$\geq4$
LogX=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d05-x01-y01
Title=ht_emu
XLabel=$H_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d H_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d06-x01-y01
Title=ht_emu ($t\bar{t}X$ subtracted)
XLabel=$H_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d H_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d07-x01-y01
Title=ht_had_emu
XLabel=$H^{\mathrm{had}}_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d H^{\mathrm{had}}_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d08-x01-y01
Title=ht_had_emu ($t\bar{t}X$ subtracted)
XLabel=$H^{\mathrm{had}}_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d H^{\mathrm{had}}_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d09-x01-y01
Title=ht_lj
XLabel=$H_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d H_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d10-x01-y01
Title=ht_lj ($t\bar{t}X$ subtracted)
XLabel=$H_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d H_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d11-x01-y01
Title=ht_had_lj
XLabel=$H^{\mathrm{had}}_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d H^{\mathrm{had}}_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d12-x01-y01
Title=ht_had_lj ($t\bar{t}X$ subtracted)
XLabel=$H^{\mathrm{had}}_{\mathrm{T}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d H^{\mathrm{had}}_{\mathrm{T}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d13-x01-y01
Title=pt_leadBjet_emu
XLabel=$p_{\mathrm{T}}^{b_1}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}}^{b_1}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d14-x01-y01
Title=pt_leadBjet_emu ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_1}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}}^{b_1}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d15-x01-y01
Title=pt_subleadBjet_emu
XLabel=$p_{\mathrm{T}}^{b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}}^{b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d16-x01-y01
Title=pt_subleadBjet_emu ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}}^{b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d17-x01-y01
Title=pt_thirdBjet_emu
XLabel=$p_{\mathrm{T}}^{b_3}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}}^{b_3}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d18-x01-y01
Title=pt_thirdBjet_emu ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_3}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}}^{b_3}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d19-x01-y01
Title=pt_leadBjet_lj
XLabel=$p_{\mathrm{T}}^{b_1}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_1}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d20-x01-y01
Title=pt_leadBjet_lj ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_1}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_1}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d21-x01-y01
Title=pt_subleadBjet_lj
XLabel=$p_{\mathrm{T}}^{b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d22-x01-y01
Title=pt_subleadBjet_lj ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d23-x01-y01
Title=pt_thirdBjet_lj
XLabel=$p_{\mathrm{T}}^{b_3}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_3}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d24-x01-y01
Title=pt_thirdBjet_lj ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_3}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_3}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d25-x01-y01
Title=pt_fourthBjet_lj
XLabel=$p_{\mathrm{T}}^{b_4}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_4}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d26-x01-y01
Title=pt_fourthBjet_lj ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}}^{b_4}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}}^{b_4}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d27-x01-y01
Title=m_bb_leading_emu
XLabel=$m_{b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d m_{b_1 b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d28-x01-y01
Title=m_bb_leading_emu ($t\bar{t}X$ subtracted)
XLabel=$m_{b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d m_{b_1 b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d29-x01-y01
Title=pt_bb_leading_emu
XLabel=$p_{\mathrm{T}, b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}, b_1 b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d30-x01-y01
Title=pt_bb_leading_emu ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}, b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T}, b_1 b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d31-x01-y01
Title=dr_bb_leading_emu
XLabel=$\Delta R_{b_1, b_2}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d \Deta R_{b_1 b_2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d32-x01-y01
Title=dr_bb_leading_emu ($t\bar{t}X$ subtracted)
XLabel=$\Delta R_{b_1, b_2}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d \Delta R_{b_1 b_2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d33-x01-y01
Title=m_bb_leading_lj
XLabel=$m_{b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d m_{b_1 b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d34-x01-y01
Title=m_bb_leading_lj ($t\bar{t}X$ subtracted)
XLabel=$m_{b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d m_{b_1 b_2}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d35-x01-y01
Title=pt_bb_leading_lj
XLabel=$p_{\mathrm{T}, b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}, b_1 b_2}}$ [GeV$^{-1}$]
LogX=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d36-x01-y01
Title=pt_bb_leading_lj ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}, b_1 b_2}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T}, b_1 b_2}}$ [GeV$^{-1}$]
LogX=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d37-x01-y01
Title=dr_bb_leading_lj
XLabel=$\Delta R_{b_1, b_2}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d \Delta R_{b_1 b_2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d38-x01-y01
Title=dr_bb_leading_lj ($t\bar{t}X$ subtracted)
XLabel=$\Delta R_{b_1, b_2}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d \Delta R_{b_1 b_2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d39-x01-y01
Title=m_bb_closest_emu
XLabel=$m_{bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d m_{bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d40-x01-y01
Title=m_bb_closest_emu ($t\bar{t}X$ subtracted)
XLabel=$m_{bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d m_{bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d41-x01-y01
Title=pt_bb_closest_emu
XLabel=$p_{\mathrm{T},bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T},bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d42-x01-y01
Title=pt_bb_closest_emu ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T},bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d p_{\mathrm{T},bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d43-x01-y01
Title=dr_bb_closest_emu
XLabel=$\Delta R_{bb}^{\Delta \mathrm{min}}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d \Delta R_{bb}^{\Delta \mathrm{min}}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d44-x01-y01
Title=dr_bb_closest_emu ($t\bar{t}X$ subtracted)
XLabel=$\Delta R_{bb}^{\Delta \mathrm{min}}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b}} \frac{d \sigma_{t\bar{t}b}}{d \Delta R_{bb}^{\Delta \mathrm{min}}}$
LogX=0
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2018_I1705857/d45-x01-y01
Title=m_bb_closest_lj
XLabel=$m_{bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d m_{bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d46-x01-y01
Title=m_bb_closest_lj ($t\bar{t}X$ subtracted)
XLabel=$m_{bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d m_{bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d47-x01-y01
Title=pt_bb_closest_lj
XLabel=$p_{\mathrm{T}, bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T},bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d48-x01-y01
Title=pt_bb_closest_lj ($t\bar{t}X$ subtracted)
XLabel=$p_{\mathrm{T}, bb}^{\Delta \mathrm{min}}$ [GeV]
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d p_{\mathrm{T},bb}^{\Delta \mathrm{min}}}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d49-x01-y01
Title=dr_bb_closest_lj
XLabel=$\Delta R_{bb}^{\Delta \mathrm{min}}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d \Delta R_{bb}^{\Delta \mathrm{min}}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1705857/d50-x01-y01
Title=dr_bb_closest_lj ($t\bar{t}X$ subtracted)
XLabel=$\Delta R_{bb}^{\Delta \mathrm{min}}$
YLabel=$\frac{1}{\sigma_{t\bar{t}b\bar{b}}} \frac{d \sigma_{t\bar{t}b\bar{b}}}{d \Delta R_{bb}^{\Delta \mathrm{min}}}$
LogX=0
LogY=0
# END PLOT