from __future__ import print_function

from glob import glob
import os
import socket
import subprocess

from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from AthenaCommon.AppMgr import theApp
import AthenaPoolCnvSvc.ReadAthenaPool
from GaudiSvc.GaudiSvcConf import THistSvc
from Rivet_i.Rivet_iConf import Rivet_i

svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
svcMgr.MessageSvc.OutputLevel = INFO
svcMgr.MessageSvc.defaultLimit = 1000000

def default_dataset_get_set(ds_name):
    # Check if we have the files
    # If we are on lxplus
    if socket.gethostname().startswith('lxplus'):
        # We don't have the data... no worries, we'll use rucio
        # to download one random file, but only if we are on lxplus!
        try:
            import rucio
        except ImportError:
            raise ImportError('You have not set rucio up!')

        subprocess.call(
            ['rucio', 'download',
             '--dir', os.environ['TMPDIR'],
             '--nrandom', '1',
             dataset])
        target = '{}/{}/*'.format(os.environ['TMPDIR'], dataset)
        svcMgr.EventSelector.InputCollections = glob(target)


# Set what dataset you want to run on here
dataset = 'mc15_13TeV.410323.Sherpa_NNPDF30NNLO_ttbb_lplush.evgen.EVNT.e5695'
# Here we guess it is in the TMPDIR but if not change the list of files that you
# give to:  `svcMgr.EventSelector.InputCollections`
target = '{}/{}/*'.format(os.environ['TMPDIR'], dataset)
svcMgr.EventSelector.InputCollections = glob(target)

# Get max events from environment because why not
if 'MAX_EVENTS' in os.environ:
    theApp.EvtMax = int(os.environ['MAX_EVENTS'])

# If in the end we didn't get anything fall back on a random file that
# we can download with rucio
if not svcMgr.EventSelector.InputCollections:
    default_dataset_get_set(dataset)

# This block sets up Rivet...
rivet = Rivet_i('rivet')
rivet.AnalysisPath = os.getcwd()
rivet.Analyses += ["ATLAS_2018_I1705857"]
rivet.HistoFile = 'out.yoda'
rivet.CrossSection = 1

# Add Rivet to the job
job = AlgSequence()
job += rivet

# This adds ROOT output if you want it
svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='out.root' OPT='RECREATE'"]
