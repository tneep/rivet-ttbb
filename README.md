Setup athena:
```bash
    asetup 21.6.3,AthGeneration,here
    source setupRivet.sh
```
	   	
``Rivet-2.6.2`` should now be set-up and ready for use
You can check if rivet is functioning properly by executing the command below, if it outputs a large list of analysis rivet is A-OK:
```bash
    rivet --list-analyses
```

Compilation of ATLAS_2018_I1705857 (or any custom analysis) by using the command below:
```bash
    rivet-buildplugin Rivet_ATLAS_2018_I1705857.so ATLAS_2018_I1705857.cc
```

To run over athena use the command below:		 
```bash
    athena.py RunRivet_test.py
```
		     
If you'd like to plot the .yoda file, do:
```bash    
    rivet-mkhtml out.yoda
```

If you'd like to plot the .yoda file, with a comparison to data, do:
```bash    
    RIVET_DATA_PATH="." rivet-mkhtml out.yoda
```

Example command for grid submitting:
```bash
    pathena RunRivet_test.py \ 
    --outDS user.sswift.410504.test \
    --inDS mc15_13TeV.410504.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad_bfil.evgen.EVNT.e5458 \
    --extOutFile out.yoda \
    --extFile=Rivet_ATLAS_2018_I1705857.so
```
