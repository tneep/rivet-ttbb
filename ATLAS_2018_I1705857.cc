#include <unordered_map>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Cutflow.hh"

namespace Rivet {


class ATLAS_2018_I1705857 : public Analysis {
 public:
  // DEFAULT_RIVET_ANA_CONSTRUCTOR(ATLAS_2018_I1705857);
  // Default constructor
  ATLAS_2018_I1705857() : Analysis("ATLAS_2018_I1705857") {}

  void init() {
    // Eta ranges
    Cut eta_full = (Cuts::abseta < 5.0);
    // Lepton cuts
    Cut lep_cuts25 = (Cuts::abseta < 2.5) && (Cuts::pT >= 25*GeV);
    // All final state particles
    FinalState fs(eta_full);

    // Get photons to dress leptons
    IdentifiedFinalState pho_id(fs);
    pho_id.acceptIdPair(PID::PHOTON);

    PromptFinalState photons(pho_id);
    photons.acceptTauDecays(true);

    // Projection to find the electrons
    IdentifiedFinalState el_id(fs);
    el_id.acceptIdPair(PID::ELECTRON);
    PromptFinalState electrons(el_id);
    electrons.acceptTauDecays(true);

    // Projection to find the muons
    IdentifiedFinalState mu_id(fs);
    mu_id.acceptIdPair(PID::MUON);
    PromptFinalState muons(mu_id);
    muons.acceptTauDecays(true);

    DressedLeptons dressedelectrons25(photons, electrons, 0.1, lep_cuts25, true);
    DressedLeptons dressedmuons25(photons, muons, 0.1, lep_cuts25, true);

    addProjection(dressedelectrons25, "electrons");
    addProjection(dressedmuons25, "muons");

    // From here on we are just setting up the jet clustering
    IdentifiedFinalState nu_id;
    nu_id.acceptNeutrinos();
    PromptFinalState neutrinos(nu_id);
    neutrinos.acceptTauDecays(true);

    VetoedFinalState jet_photons(photons);
    jet_photons.addDecayProductsVeto(15);
    jet_photons.addDecayProductsVeto(-15);

    VetoedFinalState jet_electrons(electrons);
    jet_electrons.addDecayProductsVeto(22);
    jet_electrons.addDecayProductsVeto(111);
    jet_electrons.addDecayProductsVeto(-111);

    DressedLeptons all_dressed_electrons(jet_photons, jet_electrons, 0.1, eta_full, true);
    DressedLeptons all_dressed_muons(jet_photons, muons, 0.1, eta_full, true);

    VetoedFinalState vfs(fs);
    vfs.addVetoOnThisFinalState(all_dressed_electrons);
    vfs.addVetoOnThisFinalState(all_dressed_muons);
    vfs.addVetoOnThisFinalState(neutrinos);
    addProjection(vfs, "vfs");

    FastJets jets(vfs, FastJets::ANTIKT, 0.4);
    jets.useInvisibles(JetAlg::DECAY_INVISIBLES);
    jets.useMuons(JetAlg::DECAY_MUONS);
    declare(jets, "jets");

    // We are going to book the histograms in the order that they
    // appear in the paper...

    // fiducial cross-section histogram
    _histograms["fid_xsec"] = bookHisto1D(1, 1, 1);
    _histograms["fid_xsec_no_ttX"] = bookHisto1D(2, 1, 1);

    _histograms["nbjets_emu"] = bookHisto1D(3, 1, 1);
    _histograms["nbjets_emu_no_ttX"] = bookHisto1D(4, 1, 1);

    // HT
    book_hist_emu("ht", 3, 1, 1);
    book_hist_emu("ht_had", 4, 1, 1);

    book_hist_ljets("ht", 5, 1, 1);
    book_hist_ljets("ht_had", 6, 1, 1);

    // b-jet pTs
    book_hist_emu("lead_bjet_pt", 7, 1, 1);
    book_hist_emu("sublead_bjet_pt", 8, 1, 1);
    book_hist_emu("third_bjet_pt", 9, 1, 1);

    book_hist_ljets("lead_bjet_pt", 10, 1, 1);
    book_hist_ljets("sublead_bjet_pt", 11, 1, 1);
    book_hist_ljets("third_bjet_pt", 12, 1, 1);
    book_hist_ljets("fourth_bjet_pt", 13, 1, 1);

    // leading bb pair
    book_hist_emu("m_bb_leading", 14, 1, 1);
    book_hist_emu("pt_bb_leading", 15, 1, 1);
    book_hist_emu("dR_bb_leading", 16, 1, 1);

    book_hist_ljets("m_bb_leading", 17, 1, 1);
    book_hist_ljets("pt_bb_leading", 18, 1, 1);
    book_hist_ljets("dR_bb_leading", 19, 1, 1);

    // closest bb pair
    book_hist_emu("m_bb_closest", 20, 1, 1);
    book_hist_emu("pt_bb_closest", 21, 1, 1);
    book_hist_emu("dR_bb_closest", 22, 1, 1);

    book_hist_ljets("m_bb_closest", 23, 1, 1);
    book_hist_ljets("pt_bb_closest", 24, 1, 1);
    book_hist_ljets("dR_bb_closest", 25, 1, 1);
  }


  void analyze(const Event& event) {
    double weight = event.weight();

    vector<DressedLepton> electrons = applyProjection<DressedLeptons>(event, "electrons").dressedLeptons();
    vector<DressedLepton> muons = applyProjection<DressedLeptons>(event, "muons").dressedLeptons();
    const Jets jets = applyProjection<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);

    vector<DressedLepton> leptons = overlap_removal_leptons(jets, electrons, muons);

    Jets bjets;
    foreach(const Jet& jet, jets) {
      if (jet.bTagged(Cuts::pT >= 5.0*GeV)) {
        bjets += jet;
      }
    }

    int njets = jets.size();
    int nbjets = bjets.size();

    // Evaluate basic event selection
    bool pass_ljets = (leptons.size() == 1 && leptons[0].pT() * GeV > 27);

    bool pass_emu =
      // 2 leptons > 27 GeV
      (leptons.size() == 2) &&
      (leptons[0].pT() * GeV > 27 && leptons[1].pT() * GeV > 27) &&
      // emu events
      ((leptons[0].abspid() == 11 && leptons[1].abspid() == 13) ||
       (leptons[0].abspid() == 13 && leptons[1].abspid() == 11)) &&
      // opposite charge
      (leptons[0].charge() != leptons[1].charge());

    // If we don't have exactly 1 or 2 leptons then veto the event
    if (!pass_emu && !pass_ljets) vetoEvent;

    if (pass_emu) {
      if (nbjets >= 2) {
        this->fill("nbjets_emu", nbjets - 1, weight);
      }
      if (nbjets >= 3) {
        this->fill("fid_xsec", 1, weight);
      }
      if (nbjets >= 4) {
        this->fill("fid_xsec", 2, weight);
      }
    }

    if (pass_ljets) {
      if (nbjets >= 3 && njets >= 5) {
        this->fill("fid_xsec", 3, weight);
      }
      if (nbjets >= 4 && njets >= 6) {
        this->fill("fid_xsec", 4, weight);
      }
    }

    if (pass_emu && (nbjets < 3 || njets < 3)) vetoEvent;
    if (pass_ljets && (nbjets < 4 || njets < 6)) vetoEvent;

    double hthad = sum(jets, pT, 0.0);
    double ht = sum(leptons, pT, hthad);

    FourMomentum jsum = bjets[0].momentum() + bjets[1].momentum();
    double dr_leading = deltaR(bjets[0].momentum(), bjets[1].momentum());

    std::pair<size_t, size_t> indices = get_min_dr(bjets);
    FourMomentum bb_closest =
      bjets[indices.first].momentum() + bjets[indices.second].momentum();
    double dr_closest =
      deltaR(bjets[indices.first].momentum(), bjets[indices.second].momentum());

    if (pass_ljets) {
      // b-jet pTs
      this->fill("lead_bjet_pt_ljets", bjets[0].pT() * GeV, weight);
      this->fill("sublead_bjet_pt_ljets", bjets[1].pT() * GeV, weight);
      this->fill("third_bjet_pt_ljets", bjets[2].pT() * GeV, weight);

      if (nbjets >= 4) {
	this->fill("fourth_bjet_pt_ljets", bjets[3].pT() * GeV, weight);
      }

      // HT
      this->fill("ht_ljets", ht * GeV, weight);
      this->fill("ht_had_ljets", hthad * GeV, weight);

      // leading bb pair
      this->fill("m_bb_leading_ljets", jsum.mass() * GeV, weight);
      this->fill("pt_bb_leading_ljets", jsum.pT() * GeV, weight);
      this->fill("dR_bb_leading_ljets", dr_leading, weight);

      // closest bb pair
      this->fill("m_bb_closest_ljets", bb_closest.mass() * GeV, weight);
      this->fill("pt_bb_closest_ljets", bb_closest.pT() * GeV, weight);
      this->fill("dR_bb_closest_ljets", dr_closest, weight);
    }
    if (pass_emu) {
      // b-jet pTs
      this->fill("lead_bjet_pt_emu", bjets[0].pT() * GeV, weight);
      this->fill("sublead_bjet_pt_emu", bjets[1].pT() * GeV, weight);
      this->fill("third_bjet_pt_emu", bjets[2].pT() * GeV, weight);

      // HT
      this->fill("ht_emu", ht * GeV, weight);
      this->fill("ht_had_emu", hthad * GeV, weight);

      // leading bb pair
      this->fill("m_bb_leading_emu", jsum.mass() * GeV, weight);
      this->fill("pt_bb_leading_emu", jsum.pT() * GeV, weight);
      this->fill("dR_bb_leading_emu", dr_leading, weight);

      // closest bb pair
      this->fill("m_bb_closest_emu", bb_closest.mass() * GeV, weight);
      this->fill("pt_bb_closest_emu", bb_closest.pT() * GeV, weight);
      this->fill("dR_bb_closest_emu", dr_closest, weight);
    }
  }


  void finalize() {
    // Normalise all histograms
    for (auto const& h : _histograms) {
      if (h.first == "fid_xsec") continue;
      if (h.first == "fid_xsec_no_ttX") continue;
      normalize(h.second, 1.0);
    }
    const double sf = crossSection() / femtobarn / sumOfWeights();
    scale(_histograms["fid_xsec"], sf);
    _histograms["fid_xsec"]->setAnnotation("bin1", "emu_4b");
    _histograms["fid_xsec"]->setAnnotation("bin2", "emu_3b");
    _histograms["fid_xsec"]->setAnnotation("bin3", "ljets_4b");
    _histograms["fid_xsec"]->setAnnotation("bin4", "ljets_3b");
  }

 private:
  std::unordered_map<std::string, Histo1DPtr> _histograms;

  std::pair<size_t, size_t> get_min_dr(const Jets& bjets) {
    double mindr = 999;
    std::pair<size_t, size_t> indices;
    for (size_t i = 0; i < bjets.size(); i++) {
      for (size_t j = 0; j < bjets.size(); j++) {
        if (i == j) continue;
        double dr = deltaR(bjets[i], bjets[j]);
        if (dr < mindr) {
          indices.first = i;
          indices.second = j;
          mindr = dr;
        }
      }
    }
    return indices;
  }

  vector<DressedLepton>
  overlap_removal_leptons(const Jets& jets,
                          const vector<DressedLepton>& electrons,
                          const vector<DressedLepton>& muons) {
    std::vector<DressedLepton> leptons;
    foreach(const DressedLepton& electron, electrons) {
      if (keep_lepton(jets, electron)) leptons.push_back(electron);
    }
    foreach(const DressedLepton& muon, muons) {
      if (keep_lepton(jets, muon)) leptons.push_back(muon);
    }
    return leptons;
  }

  bool keep_lepton(const Jets& jets, const DressedLepton& lepton) {
    foreach(const Jet& jet, jets) {
      if (deltaR(jet.momentum(), lepton.momentum()) < 0.4) return false;
    }
    return true;
  }

  inline void fill(const std::string& name, double value, double weight) {
    _histograms[name]->fill(value, weight);
    _histograms[name + "_no_ttX"]->fill(value, weight);
  }

  inline void book_hist_ljets(const std::string& name, int d, int x=1, int y=1) {
    // ttX substracted histograms are even numbers
    _histograms[name + "_ljets"] = bookHisto1D((d * 2) - 1, x, y);
    _histograms[name + "_ljets_no_ttX"] = bookHisto1D(d * 2, x, y);
  }

  inline void book_hist_emu(const std::string& name, int d, int x=1, int y=1) {
    // ttX substracted histograms are even numbers
    _histograms[name + "_emu"] = bookHisto1D((d * 2) - 1, x, y);
    _histograms[name + "_emu_no_ttX"] = bookHisto1D(d * 2, x, y);
  }
};

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2018_I1705857);

}  // namespace Rivet
